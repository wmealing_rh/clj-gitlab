;; Every API call to merge-requets must be authenticated.
(ns clj-gitlab.merge-requests
  (:use [clj-gitlab.core :only [api-get api-post api-put]]))

(defn- merge-requests-path [project-id]
  (str "/projects/" project-id "/merge_requests"))

(defn- merge-request-path [project-id merge-request-id]
  (str (merge-requests-path project-id) "/" merge-request-id))

(defn project-merge-requests [project-id options]
  (api-get (merge-requests-path project-id) options))

(defn merge-requests [options]
  (api-get "/merge_requests" options))

(defn merge-request [project-id merge-request-id options]
  (api-get (merge-requst-path project-id merge-request-id) options))

(defn create-merge-request [project-id params options]
  (api-post (merge-request-path project-id) params options))

(defn update-issue [project-id issue-iid params options]
  (api-put (merge-request-path project-id issue-iid) params options))
