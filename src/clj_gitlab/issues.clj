;; Every API call to issues must be authenticated.
(ns clj-gitlab.issues
  (:use [clj-gitlab.core :only [api-get api-post api-put]]))

(defn- issues-path [project-id]
  (str "/projects/" project-id "/issues"))

(defn- issue-path [project-id issue-iid]
  (str (issues-path project-id) "/" issue-iid))

(defn project-issues [project-id options]
  (api-get (issues-path project-id) options))

(defn issues [options]
  (api-get "/issues" options))

(defn issue [project-id issue-iid options]
  (api-get (issue-path project-id issue-iid) options))

(defn create-issue [project-id params options]
  (api-post (issues-path project-id) params options))

(defn update-issue [project-id issue-iid params options]
  (api-put (issue-path project-id issue-iid) params options))
