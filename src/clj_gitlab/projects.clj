(ns clj-gitlab.projects
  (:use [clj-gitlab.core :only [api-get api-post api-put]]))

(def projects-path "/projects")

(defn- project-path [id]
  (str projects-path "/" id))

(defn projects [& [options]]
  (api-get projects-path options))

(defn project [id & [options]]
  (api-get (project-path id) options))

(defn user-projects [user-id & [options]]
  (api-get (str "/users/" user-id "/projects") options))

(defn create-project [params options]
  (api-post projects-path params options))

(defn update-project [id params options]
  (api-put (project-path id) params options))
