{:calls [{:arg-key {:query-string nil,
                    :request-method :get,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects/4186769"},
          :return {:body #vcr-clj/input-stream
                   ["eyJpZCI6NDE4Njc2OSwiZGVzY3JpcHRpb24iOiJDbG9qdXJlIGNsaWVudCBmb3IgR2l0TGFiIEF"
                    "QSSIsImRlZmF1bHRfYnJhbmNoIjoibWFzdGVyIiwidGFnX2xpc3QiOltdLCJzc2hfdXJsX3RvX3"
                    "JlcG8iOiJnaXRAZ2l0bGFiLmNvbTpkemFwb3JvemhldHMvY2xqLWdpdGxhYi5naXQiLCJodHRwX"
                    "3VybF90b19yZXBvIjoiaHR0cHM6Ly9naXRsYWIuY29tL2R6YXBvcm96aGV0cy9jbGotZ2l0bGFi"
                    "LmdpdCIsIndlYl91cmwiOiJodHRwczovL2dpdGxhYi5jb20vZHphcG9yb3poZXRzL2Nsai1naXR"
                    "sYWIiLCJuYW1lIjoiY2xqLWdpdGxhYiIsIm5hbWVfd2l0aF9uYW1lc3BhY2UiOiJEbWl0cml5IF"
                    "phcG9yb3poZXRzIC8gY2xqLWdpdGxhYiIsInBhdGgiOiJjbGotZ2l0bGFiIiwicGF0aF93aXRoX"
                    "25hbWVzcGFjZSI6ImR6YXBvcm96aGV0cy9jbGotZ2l0bGFiIiwic3Rhcl9jb3VudCI6MiwiZm9y"
                    "a3NfY291bnQiOjAsImNyZWF0ZWRfYXQiOiIyMDE3LTA5LTE5VDE1OjUzOjI2LjI5MVoiLCJsYXN"
                    "0X2FjdGl2aXR5X2F0IjoiMjAxNy0wOS0yMVQxMjo0OTowOC43NDJaIn0="],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "548",
                    "Content-Type" "application/json",
                    "Date" "Thu, 21 Sep 2017 14:02:20 GMT",
                    "Etag" "W/\"43ed9ba4d3b4641906977f87524add50\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "6",
                    "RateLimit-Remaining" "594",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Request-Id" "80413973-6dc2-4145-bdd7-3679bb35705e",
                    "X-Runtime" "0.079287"},
                   :length 548,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "OK",
                   :repeatable? false,
                   :status 200,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-09-21T14:02:09.384-00:00"}
