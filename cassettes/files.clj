{:calls [{:arg-key {:query-string nil,
                    :request-method :get,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects/13083/repository/tree"},
          :return {:body #vcr-clj/input-stream
                   ["W3siaWQiOiI3YTUxNTBjOWIyMzEyMDZjOTM2OGRhYzg1NmY0OGFlZjFmOGMwYThhIiwibmFtZSI"
                    "6Ii5naXRodWIiLCJ0eXBlIjoidHJlZSIsInBhdGgiOiIuZ2l0aHViIiwibW9kZSI6IjA0MDAwMC"
                    "J9LHsiaWQiOiI2Nzk5ZTA5NTQzNTYwMDY2ZjhlYjhiYzRjMDNkNGUxYTNiOWY3YzIxIiwibmFtZ"
                    "SI6Ii5naXRsYWIiLCJ0eXBlIjoidHJlZSIsInBhdGgiOiIuZ2l0bGFiIiwibW9kZSI6IjA0MDAw"
                    "MCJ9LHsiaWQiOiIyNmVhNGIyNzUyNDcwMDNjN2Q2NzI3ZmE4YTJiNzdlYTQ5NmViMjg2IiwibmF"
                    "tZSI6ImFwcCIsInR5cGUiOiJ0cmVlIiwicGF0aCI6ImFwcCIsIm1vZGUiOiIwNDAwMDAifSx7Im"
                    "lkIjoiYTMwZGY2N2RjMjE5MTI2YThkOTBjNzc1NWRhYjkxYmUxYzdhNzgzMCIsIm5hbWUiOiJia"
                    "W4iLCJ0eXBlIjoidHJlZSIsInBhdGgiOiJiaW4iLCJtb2RlIjoiMDQwMDAwIn0seyJpZCI6ImQ1"
                    "NjRkMGJjM2RkOTE3OTI2ODkyYzU1ZTM3MDZjYzExNmQ1YjE2NWUiLCJuYW1lIjoiYnVpbGRzIiw"
                    "idHlwZSI6InRyZWUiLCJwYXRoIjoiYnVpbGRzIiwibW9kZSI6IjA0MDAwMCJ9LHsiaWQiOiI0OG"
                    "YzNWU3M2RkNGFjZTZjN2M3N2U2YWEyOGY3MmM4ODE3Nzk0YTIxIiwibmFtZSI6ImNoYW5nZWxvZ"
                    "3MiLCJ0eXBlIjoidHJlZSIsInBhdGgiOiJjaGFuZ2Vsb2dzIiwibW9kZSI6IjA0MDAwMCJ9LHsi"
                    "aWQiOiJjN2FhYzBkYmQyMWIzOTNmNGUwMWFjZTIwNTIzYTdjNGI2MjRkMzI2IiwibmFtZSI6ImN"
                    "vbmZpZyIsInR5cGUiOiJ0cmVlIiwicGF0aCI6ImNvbmZpZyIsIm1vZGUiOiIwNDAwMDAifSx7Im"
                    "lkIjoiZTFhMjQzYWEwZjU2NTJhZDdkYTdjMWY1ZDRjYTJjNmQwYmE4ZGE1ZSIsIm5hbWUiOiJkY"
                    "iIsInR5cGUiOiJ0cmVlIiwicGF0aCI6ImRiIiwibW9kZSI6IjA0MDAwMCJ9LHsiaWQiOiI2MTIw"
                    "NWM1NjZmYWUyMDc4Yjc1ODEzNzkyNzBiOTFhZjAyNGJjMzBjIiwibmFtZSI6ImRvYyIsInR5cGU"
                    "iOiJ0cmVlIiwicGF0aCI6ImRvYyIsIm1vZGUiOiIwNDAwMDAifSx7ImlkIjoiM2VmOTU5OTIxOT"
                    "gzMjE3YmI3NzkwNDEwOGM1NWJmOGZlZDVkOGE1YyIsIm5hbWUiOiJkb2NrZXIiLCJ0eXBlIjoid"
                    "HJlZSIsInBhdGgiOiJkb2NrZXIiLCJtb2RlIjoiMDQwMDAwIn0seyJpZCI6ImUzMzZkNDBmNDJk"
                    "N2M4ODIzODg3YmE1YzY5YWNjOTE3NDViMmQ0MTYiLCJuYW1lIjoiZmVhdHVyZXMiLCJ0eXBlIjo"
                    "idHJlZSIsInBhdGgiOiJmZWF0dXJlcyIsIm1vZGUiOiIwNDAwMDAifSx7ImlkIjoiM2Y1OTgzZG"
                    "Y5MzVlOTMxMjBjNDA2NGRhN2IwN2FlY2FlOTk0M2FmMyIsIm5hbWUiOiJmaXh0dXJlcyIsInR5c"
                    "GUiOiJ0cmVlIiwicGF0aCI6ImZpeHR1cmVzIiwibW9kZSI6IjA0MDAwMCJ9LHsiaWQiOiI2MTk2"
                    "YjIyMGFhYTJkNzFkMTliNTVjZWYyMjQxMmM3ZDFkYzU1NGMzIiwibmFtZSI6ImdlbmVyYXRvcl9"
                    "0ZW1wbGF0ZXMiLCJ0eXBlIjoidHJlZSIsInBhdGgiOiJnZW5lcmF0b3JfdGVtcGxhdGVzIiwibW"
                    "9kZSI6IjA0MDAwMCJ9LHsiaWQiOiJiYmVmMjlhYTM1MDAyMjAyODc5MzMyM2I4ZWM3ZTEwMWU0Y"
                    "jkwN2JjIiwibmFtZSI6ImxpYiIsInR5cGUiOiJ0cmVlIiwicGF0aCI6ImxpYiIsIm1vZGUiOiIw"
                    "NDAwMDAifSx7ImlkIjoiZDU4ZmNkZGZlYjk0ZTc0ZmNhZDAzMTUyMzZjZTdmNDJiYzY4ZDAwZCI"
                    "sIm5hbWUiOiJsb2NhbGUiLCJ0eXBlIjoidHJlZSIsInBhdGgiOiJsb2NhbGUiLCJtb2RlIjoiMD"
                    "QwMDAwIn0seyJpZCI6ImQ1NjRkMGJjM2RkOTE3OTI2ODkyYzU1ZTM3MDZjYzExNmQ1YjE2NWUiL"
                    "CJuYW1lIjoibG9nIiwidHlwZSI6InRyZWUiLCJwYXRoIjoibG9nIiwibW9kZSI6IjA0MDAwMCJ9"
                    "LHsiaWQiOiIzZmQzY2JjZTllMDg4ZGUzYzE2OGY0MGMyZDRkNjhkYTdiZmFlY2RkIiwibmFtZSI"
                    "6InB1YmxpYyIsInR5cGUiOiJ0cmVlIiwicGF0aCI6InB1YmxpYyIsIm1vZGUiOiIwNDAwMDAifS"
                    "x7ImlkIjoiYTg4MjIzZGQwY2JjMmQ1Y2RmYmVkMzA1NTc0YTJhYmM3MzFiYjc4NSIsIm5hbWUiO"
                    "iJxYSIsInR5cGUiOiJ0cmVlIiwicGF0aCI6InFhIiwibW9kZSI6IjA0MDAwMCJ9LHsiaWQiOiJl"
                    "YjMxYTAwMGI1ZjFhYmRiMTMwY2U0NzM5OGQwM2M2MjU5ODg0ZjkxIiwibmFtZSI6InJ1Ym9jb3A"
                    "iLCJ0eXBlIjoidHJlZSIsInBhdGgiOiJydWJvY29wIiwibW9kZSI6IjA0MDAwMCJ9LHsiaWQiOi"
                    "IwY2IzMjg0MWU1OTdhN2RmMjcyNTM4YWQwMmNjNzU3ZTc1YzIzODkwIiwibmFtZSI6InNjcmlwd"
                    "HMiLCJ0eXBlIjoidHJlZSIsInBhdGgiOiJzY3JpcHRzIiwibW9kZSI6IjA0MDAwMCJ9XQ=="],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "2245",
                    "Content-Type" "application/json",
                    "Date" "Tue, 03 Oct 2017 16:34:59 GMT",
                    "Etag" "W/\"c8f89c1537817fc7965c882bc7b8221f\"",
                    "Link" "<https://gitlab.com/api/v4/projects/13083/repository/tree?id=13083&page=2&per_page=20&recursive=false>; rel=\"next\", <https://gitlab.com/api/v4/projects/13083/repository/tree?id=13083&page=1&per_page=20&recursive=false>; rel=\"first\", <https://gitlab.com/api/v4/projects/13083/repository/tree?id=13083&page=4&per_page=20&recursive=false>; rel=\"last\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "1",
                    "RateLimit-Remaining" "599",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Next-Page" "2",
                    "X-Page" "1",
                    "X-Per-Page" "20",
                    "X-Prev-Page" "",
                    "X-Request-Id" "d846a814-a497-410d-b5be-b11eb9fce6d0",
                    "X-Runtime" "0.366769",
                    "X-Total" "62",
                    "X-Total-Pages" "4"},
                   :length 2245,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "OK",
                   :repeatable? false,
                   :status 200,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-10-03T16:34:57.803-00:00"}
