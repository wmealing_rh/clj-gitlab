(ns clj-gitlab.core-test
  (:require [clojure.test :refer :all]
            [clj-gitlab.core :refer :all]))

(deftest api-url-test
  (testing "building API url"
    (is (= (api-url "/projects") "https://gitlab.com/api/v4/projects"))))

(deftest parse-response-test
  (testing "parsing response"
    (let [response {:body "{\"name\":\"gitlab\"}"}]
      (is (= (parse-response response) {"name" "gitlab"})))))
